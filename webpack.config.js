const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProduction = (process.env.NODE_ENV === 'production');

const extractSass = new ExtractTextPlugin({
    filename: 'dist/style.min.css'
});

function devOrProd(development, production) {
    return isProduction ? production : development;
}

module.exports = {
    "entry":{
        "front": path.join(__dirname, './src/magicViewer.js'),
        "frame": path.join(__dirname, './src/ReceiverAPI.js')
    },
    "output": {
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/",
        filename: '[name]-bundle.js',
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules|bower_components)/,
                use: isProduction ? [
                    {
                        loader: 'babel-loader',
                        options: {
                            compact: true,
                            babelrc: false,
                            presets: ['env']
                        }
                    }
                ] : [
                    {
                        loader: 'babel-loader',
                        options: {
                            compact: true,
                            babelrc: false,
                            presets: ['env']
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        extractSass,
        new webpack.ProvidePlugin({
            'THREE': '../libs/three.module.js',
            'TWEEN': '../libs/Tween.js'
        })
    ],
    devServer: {
        host: '127.0.0.1',
        // host: '192.168.1.142',
        port: 1430,
        // inline: true,
        overlay: true,
        contentBase: path.join(__dirname, '/'),
    },
};