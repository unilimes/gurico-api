# Gurico API

An API for displaying objects on a 3D scene. Easily built into the iframe Viewer.

With this API you can easily configure the content display settings in the 3d viewer.

Read [Documentation](https://drive.google.com/file/d/1GciOUXV7cKCuWGvosrKjGLj8ThJiFvQZ/view) first.


### Ease stating

For start working with API, you need enter in your terminal/commandPrompt next commands:

- `npm i`
- `npm run build`
- open index page in browser

This repository also includes the latest build files, so after downloading the repository
to a local computer where node.js & npm is not installed, you can still start the application.

For initialization viewer you need call:

    viewer.init('json-key-1234', onSuccess, onError);
    
On initialize you can send `onSuccess` & `onError` callbacks.

In `onSuccess` callback you need initialize the api by

    function onSuccess(api) {
        api.init();
    }
    
For displaying info of some errors you may send `onError` callback like:

    function onError(e) {
        console.log('viewer error!');
    }

### Work with API events

In the current version,there is a mechanism "Subscribing to
a broadcast event -> Receiving a broadcast event".

This means that you can add a listener to a specific event.

Possible events:

- `viewer-ready` - emitted when all data is load and viewer ready to show you some stuff;
- `viewer-stop` - actually emitted when `api.stop()` was called;
- `error` - this event is emitted when something went wrong.

For adding listener on some event you need to do next things:
    
    api.addEventListener('event', () => console.log('Some event happens!'));

### Work with model as background (3Dscene mode)

For added model as background for scene you need:

- add files with model to one of directories with models, for example to **obj** directory;
- in your configuration.json you must set `type` of `background` as `"3Dscene"`;
- add configuration for this file in `background` object like this:

          "scene3D": {
            "uid": "scan_garota_v06",
            "format": "obj",
            "placement": {
              "position": {
                "x": 0,
                "y": 0,
                "z": 0
              },
              "rotation": {
                "x": 0,
                "y": 0,
                "z": 0
              },
              "scale": {
                "x": 10,
                "y": 10,
                "z": 10
              }
            }
          }
