import API from './API';

let api = new API(document.body);

let messageReceiver = function (event) {

    switch (event.data.eventHandler) {
        case 'test': api.test(); break;
        case 'api-init': api.init(event.data); break;
        case 'api-start': api.start(); break;
        case 'api-stop': api.stop(); break;
        case 'get-viewer-state': api.getViewerState(); break;
        case 'set-background': api.setBackground(event.data.backgroundObject); break;
        case 'get-background': api.getBackground(); break;
        case 'get-lights': api.getLights(); break;
        case 'get-light': api.getLight(event.data.uid); break;
        case 'add-lights': api.addLights(event.data.light); break;
        case 'remove-light': api.removeLight(event.data.uid); break;
        case 'update-light': api.updateLight(event.data.light); break;
        case 'show-advanced-rotation': api.showAdvancedRotation(event.data.isCentralizedRotation); break;
        case 'hide-advanced-rotation': api.hideAdvancedRotation(); break;
        case 'show-advanced-movement': api.showAdvancedMovement(); break;
        case 'hide-advanced-movement': api.hideAdvancedMovement(); break;
        case 'rotate': api.rotate(event.data.xdeg, event.data.ydeg, event.data.zdeg, event.data.isCentralizedRotation); break;
        case 'move':api.move(event.data.xdeg, event.data.ydeg, event.data.zdeg); break;
        case 'zoom': api.zoom(event.data.scale); break;
        case 'reset-rotation': api.resetRotation(); break;
        case 'reset-position': api.resetPosition(); break;
        case 'reset-zoom': api.resetZoom(); break;
        case 'get-screenshot': api.getScreenShot(event.data.width, event.data.height, event.data.mimetype); break;
        case 'set-fov': api.setFov(event.data.angle); break;
        case 'get-cameralook-at': api.getCameralookAt(); break;
        case 'set-cameralook-at': api.setCameraLookAt(event.data.eye, event.data.target, event.data.duration); break;
        case 'play': api.play(); break;
        case 'pause': api.pause(); break;
        case 'seek-to': api.seekTo(event.data.seconds); break;
        case 'set-volume': api.setVolume(event.data.volume); break;
        case 'get-current-time': api.getCurrentTime(); break;
        case 'get-duration': api.getDuration(); break;
        case 'set-cycle-mode': api.setCycleMode(event.data.cycleMode); break;
        case 'set-speed': api.setSpeed(event.data.speed); break;
        case 'into-fullscreen': api.intoFullScreen(); break;
        case 'out-of-fullscreen': api.outOfFullScreen(); break;
        case 'setPolygonCount': api.setPolygonCount(event.data.count); break;
        case 'getPolygonCount': api.getPolygonCount(); break;
        case 'get3DModel': api.get3DModel(); break;
        case 'reset-viewer': api.resetViewer(event.data.hardReset); break;
        case 'subscribe': api.addEventListener(event.data.event); break;
        case 'set-timing-anim': api.__setTimeAnimation(event.data.per); break;

        default: return;
    }
};

window.addEventListener('message', messageReceiver, false);
window.messRes = messageReceiver;
