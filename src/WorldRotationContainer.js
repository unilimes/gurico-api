export default class WorldRotationContainer extends THREE.Object3D {
	constructor(inner) {
		super();
		this.xContainer = new THREE.Object3D();
		this.yContainer = new THREE.Object3D();
		this.zContainer = new THREE.Object3D();

		this.add(this.zContainer);
		this.zContainer.add(this.yContainer);
		this.yContainer.add(this.xContainer);
		this.xContainer.add(inner);

	}

	rotate(x, y, z) {
		this.xContainer.rotation.x = x;
		this.yContainer.rotation.y = y;
		this.zContainer.rotation.z = z;
	}
}
