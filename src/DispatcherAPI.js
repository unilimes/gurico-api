/*
* This class for calling some methods from API class
* Use HTML5 messaging implementation.
*/

export default class DispatcherAPI {

	constructor(iframe, key, json) {
		this.iframe = iframe;
		this.json = json;
		this.key = key;
		window.__messages = {};
		window.__events = {};
		window.emitRegisteredCallback = this.emitRegisteredCallback;
		window.emitEvent = this.dispatchEvent;
	}

	registerCallbackOnMessage(message, callback) {
		window.__messages[message] = callback;
	}

	postMessage(data) {
		if (data) this.iframe.contentWindow.postMessage(data, '*');
	}

	emitRegisteredCallback(message, err, data) {
		let func = window.__messages[message];
		if (func instanceof Function) {
			if (err && data) func(new Error(err), data);
			else if (err) func(new Error(err));
			else func(data)
		}
	}

	addEventListener(event, callback) {
		if (!window.__events[event]) {
			window.__events[event] = [];
		}
		window.__events[event].push(callback);

		this.postMessage({eventHandler: 'subscribe', event: event});
	}

	dispatchEvent(event) {
		if (window.__events[event]) {
			for (let func of window.__events[event]) {
				if (func instanceof Function) func();
			}
		}
	}

	init(callback) {
		this.registerCallbackOnMessage('finish-api-init', callback);
		let data = {eventHandler: 'api-init', key: this.key, json: this.json};
		this.postMessage(data);
	}

	start(callback) {
		let data = {eventHandler: 'api-start'};
		this.registerCallbackOnMessage('api-started', callback);
		this.postMessage(data);
	}

	stop(callback) {
		let data = {eventHandler: 'api-stop'};
		this.registerCallbackOnMessage('api-stopped', callback);
		this.postMessage(data);
	}

	load(callback) {
		let data = {eventHandler: 'api-load'};
		this.registerCallbackOnMessage('api-loaded', callback);
		this.postMessage(data);
	}

	getViewerState(callback) {
		let data = {eventHandler: 'get-viewer-state'};
		this.registerCallbackOnMessage('got-viewer-state', callback);
		this.postMessage(data);
	}

	setBackground(backgroundObject, callback) {
		let data = {eventHandler: 'set-background', backgroundObject: backgroundObject};
		this.registerCallbackOnMessage('background-updated', callback);
		this.postMessage(data);
	}

	getBackground(callback) {
		let data = {eventHandler: 'get-background'};
		this.registerCallbackOnMessage('got-background', callback);
		this.postMessage(data);
	}

	getLights(callback) {
		let data = {eventHandler: 'get-lights'};
		this.registerCallbackOnMessage('got-lights', callback);
		this.postMessage(data);
	}

	getLight(uid, callback) {
		let data = {eventHandler: 'get-light', uid: uid};
		this.registerCallbackOnMessage('got-light', callback);
		this.postMessage(data);
	}

	addLights(light, callback) {
		let data = {eventHandler: 'add-lights', light: light};
		this.registerCallbackOnMessage('added-lights', callback);
		this.postMessage(data);
	}

	removeLight(uid, callback) {
		let data = {eventHandler: 'remove-light', uid: uid};
		this.registerCallbackOnMessage('removed-light', callback);
		this.postMessage(data);
	}

	updateLight(light, callback) {
		let data = {eventHandler: 'update-light', light: light};
		this.registerCallbackOnMessage('updated-light', callback);
		this.postMessage(data);
	}

	showAdvancedRotation(isCentralizedRotation, callback) {
		let data = {eventHandler: 'show-advanced-rotation', isCentralizedRotation};
		this.registerCallbackOnMessage('shown-advanced-rotation', callback);
		this.postMessage(data);
	}

	hideAdvancedRotation(callback) {
		let data = {eventHandler: 'hide-advanced-rotation'};
		this.registerCallbackOnMessage('hidden-advanced-rotation', callback);
		this.postMessage(data);
	}

	showAdvancedMovement(callback) {
		let data = {eventHandler: 'show-advanced-movement'};
		this.registerCallbackOnMessage('shown-advanced-movement', callback);
		this.postMessage(data);
	}

	hideAdvancedMovement(callback) {
		let data = {eventHandler: 'hide-advanced-movement'};
		this.registerCallbackOnMessage('hidden-advanced-movement', callback);
		this.postMessage(data);
	}

	rotate(xdeg, ydeg, zdeg, isCentralizedRotation, callback) {
		let data = {
			eventHandler: 'rotate',
			xdeg: xdeg,
			ydeg: ydeg,
			zdeg: zdeg,
			isCentralizedRotation: isCentralizedRotation
		};
		this.registerCallbackOnMessage('rotated', callback);
		this.postMessage(data);
	}

	move(xdeg, ydeg, zdeg, callback) {
		let data = {eventHandler: 'move', xdeg: xdeg, ydeg: ydeg, zdeg: zdeg};
		this.registerCallbackOnMessage('moved', callback);
		this.postMessage(data);
	}

	zoom(scale, callback) {
		let data = {eventHandler: 'zoom', scale: scale};
		this.registerCallbackOnMessage('zoomed', callback);
		this.postMessage(data);
	}

	resetRotation(callback) {
		let data = {eventHandler: 'reset-rotation'};
		this.registerCallbackOnMessage('reseted-rotation', callback);
		this.postMessage(data);
	}

	resetPosition(callback) {
		let data = {eventHandler: 'reset-position'};
		this.registerCallbackOnMessage('reseted-position', callback);
		this.postMessage(data);
	}

	resetZoom(callback) {
		let data = {eventHandler: 'reset-zoom'};
		this.registerCallbackOnMessage('reseted-zoom', callback);
		this.postMessage(data);
	}

	getScreenShot(width = this.iframe.offsetWidth, height = this.iframe.offsetHeight, mimetype = 'image/png', callback) {
		let data = {eventHandler: 'get-screenshot', width: width, height: height, mimetype: mimetype};
		this.registerCallbackOnMessage('got-screenshot', callback);
		this.postMessage(data);
	}

	setFov(angle, callback) {
		let data = {eventHandler: 'set-fov', angle: angle};
		this.registerCallbackOnMessage('seted-fov', callback);
		this.postMessage(data);
	}

	getCameralookAt(callback) {
		let data = {eventHandler: 'get-cameralook-at'};
		this.registerCallbackOnMessage('got-cameralook-at', callback);
		this.postMessage(data);
	}

	setCameraLookAt(eye, target, duration = 2000, callback) {
		let data = {eventHandler: 'set-cameralook-at', eye: eye, target: target, duration: duration};
		this.registerCallbackOnMessage('seted-cameralook-at', callback);
		this.postMessage(data);
	}

	play(callback) {
		let data = {eventHandler: 'play'};
		this.registerCallbackOnMessage('played', callback);
		this.postMessage(data);
	}

	pause(callback) {
		let data = {eventHandler: 'pause'};
		this.registerCallbackOnMessage('paused', callback);
		this.postMessage(data);
	}

	seekTo(seconds, callback) {
		let data = {eventHandler: 'seek-to', seconds: seconds};
		this.registerCallbackOnMessage('was-seek-to', callback);
		this.postMessage(data);
	}

	getCurrentTime(callback) {
		let data = {eventHandler: 'get-current-time'};
		this.registerCallbackOnMessage('got-current-time', callback);
		this.postMessage(data);
	}

	getDuration(callback) {
		let data = {eventHandler: 'get-duration'};
		this.registerCallbackOnMessage('got-duration', callback);
		this.postMessage(data);
	}

	setVolume(volume, callback) {
		let data = {eventHandler: 'set-volume', cycleMode: cycleMode};
		this.registerCallbackOnMessage('seted-volume', callback);
		this.postMessage(data);
	}

	setCycleMode(cycleMode, callback) {
		let data = {eventHandler: 'set-cycle-mode', cycleMode: cycleMode};
		this.registerCallbackOnMessage('seted-cycle-mode', callback);
		this.postMessage(data);
	}

	setSpeed(speed, callback) {
		let data = {eventHandler: 'set-speed', speed: speed};
		this.registerCallbackOnMessage('seted-speed', callback);
		this.postMessage(data);
	}

	intoFullScreen(callback) {
		let data = {eventHandler: 'into-fullscreen'};
		this.registerCallbackOnMessage('into-fullscreen', callback);
		this.postMessage(data);
	}

	outOfFullScreen(callback) {
		let data = {eventHandler: 'out-of-fullscreen'};
		this.registerCallbackOnMessage('out-of-fullscreen', callback);
		this.postMessage(data);
	}

	setPolygonCount(count, callback) {
		let data = {eventHandler: 'setPolygonCount', count: count};
		this.registerCallbackOnMessage('setPolygonCount', callback);
		this.postMessage(data);
	}

	getPolygonCount(callback) {
		let data = {eventHandler: 'getPolygonCount'};
		this.registerCallbackOnMessage('getPolygonCount', callback);
		this.postMessage(data);
	}

	get3DModel(callback) {
		let data = {eventHandler: 'get3DModel'};
		this.registerCallbackOnMessage('get3DModel', callback);
		this.postMessage(data);
	}

	resetViewer(hardReset = true, callback) {
		let data = {eventHandler: 'reset-viewer', hardReset: hardReset};
		this.registerCallbackOnMessage('reseted-viewer', callback);
		this.postMessage(data);
	}

}

window.addEventListener('message', function (event) {
	if (event.data.callbackHandler) {
		window.emitRegisteredCallback(event.data.callbackHandler, event.data.err, event.data.dropped);
	} else if (event.data.broadcastEvent) window.emitEvent(event.data.broadcastEvent);
}, false);
