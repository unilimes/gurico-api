import DispatcherAPI from "./DispatcherAPI";

class Magic3DViewer {
	constructor(container = body) {
		this.container = container;
		this.iframeContainer = document.createElement('iframe');
		this.iframeContainer.src = 'viewer.html';
		this.iframeContainer.setAttribute('allowFullScreen', '');
		this.container.appendChild(this.iframeContainer);
		this.iframeContainer.width = this.container.offsetWidth;
		this.iframeContainer.height = this.container.offsetHeight;
	}

	_loadJSON(key) {
		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();
			xhr.open('GET', `data/${key}.json`, true);

			xhr.onload = function () {
				if (this.status == 200) {
					resolve(this.response);
				} else {
					let error = new Error(this.statusText);
					error.code = this.status;
					reject(error);
				}
			};
			xhr.onerror = () => reject(new Error("Network Error"));
			xhr.send();
		})
	}

	init(key, onSuccess, onError) {
		this._loadJSON(key).then(
			json => {
				this.json = JSON.parse(json);
				this.key = key;
				this.api = new DispatcherAPI(this.iframeContainer, this.key, this.json);
				this.iframeContainer.onload = () => onSuccess(this.api);
				this.api.addEventListener('error', onError)
			},
			error => onError()
		);
	}
}

window.Magic3DViewer = Magic3DViewer;
