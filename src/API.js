import '../libs/OrbitControls';
import '../libs/TransformControls';
import '../libs/OBJLoader';
import '../libs/MTLLoader';
import '../libs/FBXLoader';
import '../libs/SimplifyModifier';
import '../libs/ColladaLoader2';
import {eachSeries, series} from 'async';
import PositioningHelper from './PositioningHelper';
import Preloader from './Preloader';
import WorldRotationContainer from "./WorldRotationContainer";

export default class API {

	constructor(viewer) {
		this.container = viewer;
		this.__events = [];
		this.__background = {};
		this.__lightsList = [];
		this.__modelsList = [];
		this.__viewerObjects = {
			cur_background: {},
		};
		this.__animationsControllers = [];
		this.clock = new THREE.Clock();
		this.parent = window.top;
		this.animationEnabled = false;
		this.centralizedRotation = false;
	}

	addEventListener(event) {
		if (!this.__events.includes(event)) {
			this.__events.push(event);
		}
	}

	emitEvent(event, next) {
		if (this.__events.includes(event)) {
			this.parent.postMessage({broadcastEvent: event}, '*');
		}
		if (next && next instanceof Function) next();
	}

	loadEnvironment(callback) {
		let cameraController = this.__json.scene.cameraController;
		this.objectContainer = new THREE.Object3D();
		this.objectContainer.name = 'container';
		this.worldRotationContainer = new WorldRotationContainer(this.objectContainer);
		this.scene.add(this.worldRotationContainer);

		let worldContainer = this.__json.scene.worldContainer;
		if (worldContainer) {
			this.objectContainer.position.set(worldContainer.placement.position.x, worldContainer.placement.position.y, worldContainer.placement.position.z);
			this.rotate(worldContainer.placement.rotation.x, worldContainer.placement.rotation.y, worldContainer.placement.rotation.z, worldContainer.placement.centralizedRotation);
		}

		const audioData = this.__json.scene.audio;
		if (audioData) {
			this.syncedAudioElements = [];
			this.unsyncedAudioElements = [];

			audioData.items.forEach(item => {
				const element = document.createElement("audio");
				switch (item.format) {
					case "mpeg": {
						element.src = `${item.uid}.mp3`;
						element.type = `audio/mpeg`;
						break;
					}
					case "ogg": {
						element.src = `${item.uid}.ogg`;
						element.type = `audio/ogg`;
						break;
					}
					case "wav": {
						element.src = `${item.uid}.wav`;
						element.type = `audio/wav`;
						break;
					}
				}
				element.loop = item.isLooped;

				if (item.isSynced) this.syncedAudioElements.push(element);
				else this.unsyncedAudioElements.push(element);
			});

			if (this.unsyncedAudioElements.length)
				this.unsyncedAudioElements.forEach(unsyncedAudio => unsyncedAudio.play());
		}

		this.setBackground(this.__json.scene.background, updatedBG => {
			this.__background = updatedBG;
		});

		this.camera.position.set(1200, 0, 1000);

		if (cameraController) {
			this.camera.rotation.set(
				cameraController.placement.rotation.x,
				cameraController.placement.rotation.y,
				cameraController.placement.rotation.z
			);

			this.orbitControls.maxDistance = cameraController.maxDistance;
			this.orbitControls.minDistance = cameraController.minDistance;

			this.camera.lookAt(
				cameraController.placement.target.x,
				cameraController.placement.target.y,
				cameraController.placement.target.z
			);
		} else {
			this.camera.rotation.set(0, 0, 0);
			this.orbitControls.maxDistance = 4000;
			this.orbitControls.minDistance = 0;
			this.camera.lookAt(0, 0, 0);
		}

		this.__json.scene.lights.items.forEach(light => {
			this.addLights(light);
		});

		if (callback instanceof Function) callback();
	}

	loadingOneModel(model, parent, next) {
		if (model.format === 'json') {
			this.objectLoader.load(`${model.uid}.${model.format}`, json => {

				let lowerCrop;
				let upperCrop;

				if (model.placement.crop) {
					lowerCrop = new THREE.Plane(new THREE.Vector3(0, 1, 0), model.placement.crop.lower);
					upperCrop = new THREE.Plane(new THREE.Vector3(0, -1, 0), model.placement.crop.upper);
				}

				if (json.material instanceof Array) {
					json.material.forEach(material => {
						material.side = THREE.DoubleSide;
						material.transparent = true;
						material.opacity = 1;
						if (lowerCrop || upperCrop) {
							material.clippingPlanes = [lowerCrop, upperCrop];
						}
					});
				} else {
					json.material.side = THREE.DoubleSide;
					json.material.transparent = true;
					json.material.opacity = 1;
					if (lowerCrop || upperCrop) {
						json.material.clippingPlanes = [lowerCrop, upperCrop];
					}
				}

				json.needCenteringPosition = model.placement.needCenteringPosition;
				json.position.set(model.placement.position.x, model.placement.position.y, model.placement.position.z);
				json.rotation.set(model.placement.rotation.x, model.placement.rotation.y, model.placement.rotation.z);
				json.scale.x = model.placement.scale.x;
				json.scale.y = model.placement.scale.y;
				json.scale.z = model.placement.scale.z;

				if (json instanceof THREE.Group) {
					json.traverse(child => {
						if (child instanceof THREE.Mesh) {
							child.initialGeometry = child.geometry;
							if (child.geometry instanceof THREE.BufferGeometry) {
								child.convertedGeometry = new THREE.Geometry().fromBufferGeometry(child.geometry);
							} else {
								child.convertedGeometry = child.geometry;
							}
						}
					});
				} else {
					json.initialGeometry = json.geometry;
					if (json.geometry instanceof THREE.BufferGeometry) {
						json.convertedGeometry = new THREE.Geometry().fromBufferGeometry(json.geometry);
					} else {
						json.convertedGeometry = json.geometry;
					}
				}

				json.uid = model.uid;
				json.customType = "gur_object";
				this.__modelsList.push(model);
				if (parent) parent.add(json);
				else this.scene.add(json);
				if (next) next();
			}, t => {
			}, error => this.emitEvent('error'));
		}
		if (model.format === 'obj') {

			let path = model.uid.substr(0, model.uid.lastIndexOf('/') + 1);
			let fileName = model.uid.substr(model.uid.lastIndexOf('/') + 1, model.uid.length);

			this.MTLloader.setPath(path);
			this.MTLloader.setCrossOrigin('*');
			this.MTLloader.load(`${fileName}.mtl`, materials => {
				materials.preload();
				this.OBJloader.setMaterials(materials);
				this.OBJloader.setPath(path);
				this.OBJloader.load(`${fileName}.obj`, obj => {
					let objectBB = PositioningHelper.calcBoundingBox(obj);
					PositioningHelper.normalizePosition(obj, objectBB);
					if (model.placement.crop) {
						let lowerCrop = new THREE.Plane(new THREE.Vector3(0, 1, 0), model.placement.crop.lower);
						let upperCrop = new THREE.Plane(new THREE.Vector3(0, -1, 0), model.placement.crop.upper);

						obj.traverse(child => {
							if (child instanceof THREE.Mesh) {
								child.material.clippingPlanes = [lowerCrop, upperCrop];

								setTimeout(() => {
									child.material.side = THREE.DoubleSide;
									child.material.needsUpdate = true;
								}, 1000);
								child.material.transparent = true;
								child.material.opacity = 1;
							}
						});
					} else {
						obj.traverse(child => {
							if (child instanceof THREE.Mesh) {
								setTimeout(() => {
									child.material.side = THREE.DoubleSide;
									child.material.needsUpdate = true;
								}, 1000);
								child.material.transparent = true;
								child.material.opacity = 1;
							}
						});
					}
					obj.needCenteringPosition = model.placement.needCenteringPosition;
					obj.position.set(model.placement.position.x, model.placement.position.y, model.placement.position.z);
					obj.rotation.set(model.placement.rotation.x, model.placement.rotation.y, model.placement.rotation.z);
					obj.scale.x = model.placement.scale.x;
					obj.scale.y = model.placement.scale.y;
					obj.scale.z = model.placement.scale.z;

					obj.bb = PositioningHelper.calcBoundingBox(obj);
					this.transformControls.setSize(obj.bb.dem.x / 500);

					obj.uid = model.uid;
					obj.customType = "gur_object";
					this.__modelsList.push(model);

					obj.traverse(child => {
						if (child instanceof THREE.Mesh) {
							child.initialGeometry = child.geometry;
							if (child.geometry instanceof THREE.BufferGeometry) {
								child.convertedGeometry = new THREE.Geometry().fromBufferGeometry(child.geometry);
							} else {
								child.convertedGeometry = child.geometry;
							}
						}
					});

					if (parent) parent.add(obj);
					else this.scene.add(obj);

					if (next) next();
				}, t => {
				}, error => this.emitEvent('error'));
			}, t => {
			}, error => this.emitEvent('error'));
		}
		if (model.format === 'fbx') {
			let manager = new THREE.LoadingManager();
			let loader = new THREE.FBXLoader(manager);

			loader.load(`${model.uid}.${model.format}`, fbx => {
				fbx.mixer = new THREE.AnimationMixer(fbx);
				let action = fbx.mixer.clipAction(fbx.animations[0]);
				this.__animationsControllers.push({"uid": model.uid, "mixer": fbx.mixer, "action": action});
				this.renderer.localClippingEnabled = true;

				if (model.placement.crop) {
					let lowerCrop = new THREE.Plane(new THREE.Vector3(0, 1, 0), model.placement.crop.lower);
					let upperCrop = new THREE.Plane(new THREE.Vector3(0, -1, 0), model.placement.crop.upper);

					fbx.traverse(child => {
						if (child instanceof THREE.Mesh) {
							child.material.clippingPlanes = [lowerCrop, upperCrop];
							child.material.side = THREE.DoubleSide;
							child.material.transparent = true;
							child.material.opacity = 1;
						}
					});
				} else {
					fbx.traverse(child => {
						if (child instanceof THREE.Mesh) {
							setTimeout(() => {
								child.material.side = THREE.DoubleSide;
								child.material.needsUpdate = true;
							}, 1000);
							child.material.transparent = true;
							child.material.opacity = 1;
						}
					});
				}

				fbx.needCenteringPosition = model.placement.needCenteringPosition;
				fbx.position.set(model.placement.position.x, model.placement.position.y, model.placement.position.z);
				fbx.rotation.set(model.placement.rotation.x, model.placement.rotation.y, model.placement.rotation.z);
				fbx.scale.x = model.placement.scale.x;
				fbx.scale.y = model.placement.scale.y;
				fbx.scale.z = model.placement.scale.z;

				fbx.uid = model.uid;
				fbx.customType = "gur_object";
				this.__modelsList.push(model);

				fbx.traverse(child => {
					if (child instanceof THREE.Mesh) {
						child.initialGeometry = child.geometry;
						if (child.geometry instanceof THREE.BufferGeometry) {
							child.convertedGeometry = new THREE.Geometry().fromBufferGeometry(child.geometry);
						} else {
							child.convertedGeometry = child.geometry;
						}
					}
				});

				if (parent) parent.add(fbx);
				else this.scene.add(fbx);
				if (next) next();
			});
		}
		if (model.format === 'dae') {

			let loadingManager = new THREE.LoadingManager();
			let loader = new THREE.ColladaLoader(loadingManager);

			loader.load(`${model.uid}.${model.format}`, collada => {
				let modelData = collada.scene.children[0];
				modelData.geometry.computeBoundingSphere();

				let lowerCrop;
				let upperCrop;

				if (model.placement.crop) {
					lowerCrop = new THREE.Plane(new THREE.Vector3(0, 1, 0), model.placement.crop.lower);
					upperCrop = new THREE.Plane(new THREE.Vector3(0, -1, 0), model.placement.crop.upper);
				}

				if (modelData.material instanceof Array) {
					modelData.material.forEach(material => {
						material.side = THREE.DoubleSide;
						material.transparent = true;
						material.opacity = 1;
						if (lowerCrop || upperCrop) {
							material.clippingPlanes = [lowerCrop, upperCrop];
						}
					});
				} else {
					modelData.material.side = THREE.DoubleSide;
					modelData.material.transparent = true;
					modelData.material.opacity = 1;
					if (lowerCrop || upperCrop) {
						modelData.material.clippingPlanes = [lowerCrop, upperCrop];
					}
				}

				modelData.needCenteringPosition = model.placement.needCenteringPosition;
				modelData.position.set(model.placement.position.x, model.placement.position.y, model.placement.position.z);
				modelData.rotation.set(model.placement.rotation.x, model.placement.rotation.y, model.placement.rotation.z);
				modelData.scale.x = model.placement.scale.x;
				modelData.scale.y = model.placement.scale.y;
				modelData.scale.z = model.placement.scale.z;

				modelData.uid = model.uid;
				modelData.customType = "gur_object";
				this.__modelsList.push(model);

				if (modelData instanceof THREE.Group) {
					modelData.traverse(child => {
						if (child instanceof THREE.Mesh) {
							child.initialGeometry = child.geometry;
							if (child.geometry instanceof THREE.BufferGeometry) {
								child.convertedGeometry = new THREE.Geometry().fromBufferGeometry(child.geometry);
							} else {
								child.convertedGeometry = child.geometry;
							}
						}
					});
				} else {
					modelData.initialGeometry = modelData.geometry;
					modelData.convertedGeometry = new THREE.Geometry().fromBufferGeometry(modelData.geometry);
				}

				modelData.geometry.elementsNeedUpdate = true;

				if (parent) parent.add(modelData);
				else this.scene.add(modelData);
				if (next) next();
			});
		}
	}

	loadModels(callback) {
		eachSeries(this.__json.models.items, (model, next) => {
			this.loadingOneModel(model, this.objectContainer, next);
		}, () => {
			this.objectContainer.traverse(model => {
				if (model.customType === "gur_object") {
					if (model instanceof THREE.Mesh) API.__adjustPosition(model, 'mesh');
					else if (model instanceof THREE.Group) API.__adjustPosition(model.children[0], 'group');
					model.rotation.set(0, 0, 0);
				}
			});
			callback();
		});
	}

	init(data) {

		series([
			callback => {
				this.preloader = new Preloader(document.getElementById('preloader'));
				this.preloader.show(callback)
			},
			callback => {
				if (!this.isInited) {
					this.key = data.key;
					this.__json = data.json;

					this.renderer = new THREE.WebGLRenderer({
						antialias: true,
						alpha: true,
						preserveDrawingBuffer: true
					});
					this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
					this.renderer.domElement.style.position = 'absolute';
					this.renderer.localClippingEnabled = true;
					this.aspectRatio = this.container.offsetWidth / this.container.offsetHeight;
					document.body.appendChild(this.renderer.domElement);

					if (this.__json.scene.cameraController) {
						this.camera = new THREE.PerspectiveCamera(
							this.__json.scene.cameraController.camera.fov,
							this.aspectRatio,
							this.__json.scene.cameraController.camera.near,
							this.__json.scene.cameraController.camera.far
						);
					} else {
						this.camera = new THREE.PerspectiveCamera(50, this.aspectRatio, 0.1, 2000);
					}

					this.scene = new THREE.Scene();

					this.orbitControls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
					this.transformControls = new THREE.TransformControls(this.camera, this.renderer.domElement);


					this.gridHelper = new THREE.GridHelper(2000, 40);
					this.gridHelper.visible = false;
					this.scene.add(this.gridHelper);

					this.axesHelper = new THREE.AxesHelper(1000);
					this.scene.add(this.axesHelper);
					this.axesHelper.visible = false;

					this.scene.add(this.transformControls);
					this.transformControls.visible = false;
					this.OBJloader = new THREE.OBJLoader();
					this.MTLloader = new THREE.MTLLoader();
					this.objectLoader = new THREE.ObjectLoader();
					this.modifer = new THREE.SimplifyModifier();

					window.addEventListener('resize', () => {

						this.camera.aspect = window.innerWidth / window.innerHeight;
						this.camera.updateProjectionMatrix();

						this.renderer.setSize(window.innerWidth, window.innerHeight);

					}, false);

					this.transformControls.addEventListener('rotation', (event) => {

						this.centralizedRotation = false;

					}, false);

					this.isInited = true;
					callback();
				}
			},
			callback => this.loadEnvironment(callback),
			callback => this.loadModels(callback),
			callback => {
				if (!this.__json.scene.cameraController) {
					this.objectContainer.children[0].children[0].geometry.computeBoundingBox();
					const bbox = this.objectContainer.children[0].children[0].geometry.boundingBox;
					let center = bbox.getCenter();
					this.objectContainer.children[0].children[0].position.sub(center);
					let size = new THREE.Vector3().subVectors(bbox.max, bbox.min);
					this.camera.position.set(0, 0, 3 * Math.max(Math.abs(size.x), Math.max(Math.abs(size.y), Math.abs(size.z))));
					this.camera.far = 10 * this.camera.position.z;
					this.camera.updateProjectionMatrix();
					this.orbitControls.update();
					callback();
				} else callback();
			},
			callback => this.__animate(callback),
			callback => this.preloader.hide(callback),
			callback => this.emitEvent('viewer-ready', callback),
			callback => {
				if (this.__json.scene.cameraController) {
					this.setCameraLookAt(
						{"x": 500, "y": 100, "z": 1200},
						{"x": 10000, "y": 100, "z": -1200},
						3000, callback
					);
				} else {
					this.objectContainer.children[0].children[0].geometry.computeBoundingBox();
					const bbox = this.objectContainer.children[0].children[0].geometry.boundingBox;
					let center = bbox.getCenter();
					this.objectContainer.children[0].children[0].position.sub(center);
					let size = new THREE.Vector3().subVectors(bbox.max, bbox.min);
					this.camera.position.set(0, 0, 3 * Math.max(Math.abs(size.x), Math.max(Math.abs(size.y), Math.abs(size.z))));
					this.camera.far = 10 * this.camera.position.z;
					this.camera.updateProjectionMatrix();
					this.orbitControls.update();
					callback();
				}
			},

		]), () => this.parent.postMessage({callbackHandler: 'finish-api-init'}, '*');
	};

	_render() {
		if (this.__animationsControllers && this.__animationsControllers.length > 0) {
			this.__animationsControllers.forEach(controller => {
				controller.mixer.update(this.clock.getDelta());
				if (controller.action) {
					this.syncedAudioElements.forEach(el => {
						if(el.currentTime > controller.action._clip.duration) {
							if(el.loop) el.currentTime = controller.action.time;
							else el.pause();
						}
					});
				}
			});
		}
		this.orbitControls.update();
		this.transformControls.update();
		TWEEN.update();
		this.renderer.render(this.scene, this.camera);
	}

	__timePrepare(time) {
		let t = new Date(parseInt(time));
		let mm = t.getUTCMinutes();
		mm = mm < 10 ? '0' + mm : mm;
		let ss = t.getUTCSeconds();
		ss = ss < 10 ? '0' + ss : ss;
		let ms = (t.getUTCMilliseconds() / 10).toFixed(0);
		ms = ms < 10 ? '0' + ms : ms;

		return `${mm}:${ss}:${ms}`;
	}

	__tcWrapper(f, that) {
		return function () {
			let err = '';
			try {
				f.apply(that, arguments);
			} catch (e) {
				that.emitEvent('error');
				err = e.message;
			}
			return err;
		}
	}

	static __adjustPosition(mesh, type) {
		switch (type) {
			case 'group': {
				mesh.geometry.computeBoundingSphere();
				const center = mesh.geometry.boundingSphere.center;
				mesh.parent.needCenteringPosition
					? mesh.parent.position.set(center.x * -1, center.y * -1, center.z * -1)
					: mesh.parent.position.set(0, 0, 0);
				break;
			}
			case 'mesh': {
				mesh.geometry.computeBoundingSphere();
				const center = mesh.geometry.boundingSphere.center;
				mesh.needCenteringPosition
					? mesh.position.set(center.x * -1, center.y * -1, center.z * -1)
					: mesh.position.set(0, 0, 0);
				break;
			}
			default: {
				mesh.geometry.computeBoundingSphere();
				const center = mesh.geometry.boundingSphere.center;
				mesh.parent.needCenteringPosition
					? mesh.parent.position.set(center.x * -1, center.y * -1, center.z * -1)
					: mesh.parent.position.set(0, 0, 0);
				break;
			}
		}
	}

	__animate(callback) {

		let __animate = () => {

			requestAnimationFrame(() => {
				__animate();
			});


			this._render();


			if (play) {
				if (this.__animationsControllers[0]) {
					playerCurrentTime.innerText = this.__timePrepare((this.__animationsControllers[0].action.time * 1000).toFixed(0));
					$("#animationSlider").slider('option', 'value', ((this.__animationsControllers[0].action.time.toFixed(3) / this.__animationsControllers[0].action._clip.duration) * 99).toFixed(5));
				}
			}
		};
		__animate();

		if (callback instanceof Function) callback();
	};

	start() {
		this.objectContainer.traverse(child => {
			if (child instanceof THREE.Mesh) {
				child.material.opacity = 1;
			}
		});

		if (this.__animationsControllers[0] && this.__animationsControllers[0].action) {
			this.__animationsControllers[0].action.play();
			if (!this.animationEnabled) {
				this.play();
				this.animationEnabled = true;
				window.animationLength = this.__animationsControllers[0].action._clip.duration;
				this.parent.postMessage({callbackHandler: 'api-started'}, '*');
			}
		}
	}

	stop() {
		this.__animationsControllers[0].action.stop();
		if (this.animationEnabled) {
			this.pause();
			this.animationEnabled = false;
			this.parent.postMessage({callbackHandler: 'api-stopped'}, '*');
		}
	}

	load() {
		this.parent.postMessage({callbackHandler: 'api-loaded'}, '*');
	}

	getViewerState() {
		let data = {
			callbackHandler: 'got-viewer-state',
			dropped: {
				"scene": {
					"background": this.__background,
					"cameraController": {
						"placement": {
							"position": {
								x: this.camera.position.x,
								y: this.camera.position.y,
								z: this.camera.position.z
							},
							"rotation": {
								x: this.camera.rotation.x,
								y: this.camera.rotation.y,
								z: this.camera.rotation.z
							},
							"target": this.orbitControls.target,
						},
						"camera": {
							"fov": this.camera.fov,
							"near": this.camera.near,
							"far": this.camera.far
						},
						"minDistance": this.orbitControls.minDistance,
						"maxDistance": this.orbitControls.maxDistance
					},
					"lights": {
						"items": this.__lightsList
					},
					"worldContainer": {
						"placement": {
							"centralizedRotation": this.centralizedRotation,
							"position": {
								"x": this.objectContainer.position.x,
								"y": this.objectContainer.position.y,
								"z": this.objectContainer.position.z
							},
							"rotation": {
								"x": this.centralizedRotation ? this.worldRotationContainer.xContainer.rotation.x : this.objectContainer.rotation.x,
								"y": this.centralizedRotation ? this.worldRotationContainer.yContainer.rotation.y : this.objectContainer.rotation.y,
								"z": this.centralizedRotation ? this.worldRotationContainer.zContainer.rotation.z : this.objectContainer.rotation.z
							},
							"scale": {
								"x": this.objectContainer.scale.x,
								"y": this.objectContainer.scale.y,
								"z": this.objectContainer.scale.z
							}
						}
					}
				},
				"models": this.__modelsList
			}
		};
		this.parent.postMessage(data, '*');
	}

	setBackground(backgroundObject) {

		this.container.style.backgroundColor = '';
		this.container.style.background = '';

		if (this.__viewerObjects.cur_background.obj) {
			if (this.__viewerObjects.cur_background.type === 'image') {
				this.container.removeChild(this.__viewerObjects.cur_background.obj);
			} else if (this.__viewerObjects.cur_background.type === 'panorama' || this.__viewerObjects.cur_background.type === '3Dscene') {
				this.__viewerObjects.cur_background.obj.material.visible = false;
			}
		}

		let updatedBG = {};

		if (backgroundObject.type === 'color') {
			let newBgColor = {
				r: parseInt(backgroundObject.color.r * 256),
				g: parseInt(backgroundObject.color.g * 256),
				b: parseInt(backgroundObject.color.b * 256),
				a: backgroundObject.color.a
			};
			this.container.style.backgroundColor = `rgba(${newBgColor.r}, ${newBgColor.g}, ${newBgColor.b}, ${newBgColor.a})`;
			updatedBG.color = backgroundObject.color;
		}

		if (backgroundObject.type === 'gradient' && backgroundObject.gradient) {
			let from = {
				r: parseInt(backgroundObject.gradient.from.r * 256),
				g: parseInt(backgroundObject.gradient.from.g * 256),
				b: parseInt(backgroundObject.gradient.from.b * 256),
				a: backgroundObject.gradient.from.a
			};
			let to = {
				r: parseInt(backgroundObject.gradient.to.r * 256),
				g: parseInt(backgroundObject.gradient.to.g * 256),
				b: parseInt(backgroundObject.gradient.to.b * 256),
				a: backgroundObject.gradient.to.a
			};

			this.container.style.background = `linear-gradient(
            rgba(${from.r}, ${from.g}, ${from.b}, ${from.a}),
            rgba(${to.r}, ${to.g}, ${to.b}, ${to.a}))`;
			updatedBG.gradient = backgroundObject.gradient;
		}

		if (backgroundObject.type === 'image') {
			let image = document.createElement('img');
			image.width = this.container.offsetWidth;
			image.height = this.container.offsetHeight;
			image.src = backgroundObject.image_uid;
			image.style.filter = `blur(${backgroundObject.blur * 3}px)`;
			image.style.position = `absolute`;
			this.container.insertBefore(image, this.container.children[0]);
			this.__viewerObjects.cur_background = {obj: image, type: 'image'};

			updatedBG.image_uid = backgroundObject.image_uid;
			updatedBG.blur = backgroundObject.blur;
		}

		if (backgroundObject.type === 'panorama') {
			let geometry = new THREE.SphereBufferGeometry(4000, 60, 40);
			geometry.scale(-1, 1, 1);

			let bluredCanvas = document.createElement('canvas');
			let img = new Image();

			let blurFactor = backgroundObject.blur * 3 + 1;
			let cutFactor = blurFactor*3;

			img.onload = () => {
				bluredCanvas.width = img.width;
				bluredCanvas.height = img.height;

				let tempCanvas = document.createElement('canvas');
				tempCanvas.width = img.width + blurFactor*2;
				tempCanvas.height = img.height + blurFactor*2;
				let tempCtx = tempCanvas.getContext( '2d' );

				tempCtx.filter = `blur(${blurFactor}px)`;
				tempCtx.drawImage( img, 0, 0, tempCanvas.width, tempCanvas.height);
				let context = bluredCanvas.getContext('2d');

				context.drawImage( tempCanvas, 															//source
					cutFactor, cutFactor, tempCanvas.width-cutFactor*2, tempCanvas.height-cutFactor*2,	// from
					0, 0, bluredCanvas.width, bluredCanvas.height 										// to
				);

				let nextTexture = new THREE.Texture(bluredCanvas);
					nextTexture.minFilter = THREE.NearestMipMapLinearFilter;

				let material = new THREE.MeshBasicMaterial({ map: nextTexture });
				material.map.needsUpdate = true;
				let panorama = new THREE.Mesh(geometry, material);
				this.scene.add(panorama);

				updatedBG.panoramic_uid = backgroundObject.panoramic_uid;
				this.__viewerObjects.cur_background = {obj: panorama, type: 'panorama'};
			};

			img.crossOrigin = 'anonymous';
			img.src = backgroundObject.panoramic_uid;
		}

		if (backgroundObject.type === '3Dscene') {
			this.loadingOneModel(backgroundObject.scene3D, this.scene);
		}


		updatedBG.type = backgroundObject.type;

		this.__background = updatedBG;
		let data = {callbackHandler: 'background-updated', dropped: updatedBG};
		this.parent.postMessage(data, '*');
	}

	getBackground() {
		this.parent.postMessage({callbackHandler: 'got-background', dropped: this.__background}, '*');
	}

	getLights() {
		this.parent.postMessage({callbackHandler: 'got-lights', dropped: {"items": this.__lightsList}}, '*');
	}

	getLight(uid) {
		let data = {};
		this.__lightsList.forEach(light => {
			if (light.uid === uid) {
				data = {
					callbackHandler: 'got-light',
					dropped: light
				};
			}
		});
		this.parent.postMessage(data, '*');
	}

	addLights(light) {
		let newLight = {};
		if (light.type === 'hemisphere') {
			newLight = new THREE.HemisphereLight(
				new THREE.Color(light.skyColor.r, light.skyColor.g, light.skyColor.b),
				new THREE.Color(light.groundColor.r, light.groundColor.g, light.groundColor.b),
				light.intensity
			);
		}

		if (light.type === 'directional') {
			newLight = new THREE.DirectionalLight(
				new THREE.Color(light.color.r, light.color.g, light.color.b),
				light.intensity
			);
		}

		if (light.type === 'ambient') {
			newLight = new THREE.AmbientLight(
				new THREE.Color(light.color.r, light.color.g, light.color.b),
				light.intensity
			);
		}

		newLight.castShadow = light.castShadow || false;

		if (!newLight.isAmbientLight) newLight.position.set(
			light.position.x || 0,
			light.position.y || 0,
			light.position.z || 0
		);

		newLight.visible = light.showLight || false;
		newLight.uid = light.uid;
		newLight.positioning = light.positioning;

		this.__lightsList.push(light);
		this.scene.add(newLight);

		let data = {
			callbackHandler: 'added-lights',
			dropped: {"items": this.__lightsList}
		};

		this.parent.postMessage(data, '*');
	}

	removeLight(uid) {
		this.__lightsList.forEach((item, key) => {
			if (item.uid === uid) this.__lightsList.splice(key, 1);
			return
		});

		let removed_light = {};
		this.scene.traverse(child => {
			if (child.uid === uid) removed_light = child
		});
		this.scene.remove(removed_light);
		this.parent.postMessage({callbackHandler: 'removed-light', dropped: {"items": this.__lightsList}}, '*');
	}

	updateLight(light) {
		let newLight;
		this.removeLight(light.uid);

		if (light.type === 'hemisphere') {
			newLight = new THREE.HemisphereLight(
				new THREE.Color(light.skyColor.r, light.skyColor.g, light.skyColor.b),
				new THREE.Color(light.groundColor.r, light.groundColor.g, light.groundColor.b),
				light.intensity
			);
		}

		if (light.type === 'directional') {
			newLight = new THREE.DirectionalLight(
				new THREE.Color(light.color.r, light.color.g, light.color.b),
				light.intensity
			);
		}

		if (light.type === 'ambient') {
			newLight = new THREE.AmbientLight(
				new THREE.Color(light.color.r, light.color.g, light.color.b),
				light.intensity
			);
		}

		if (!newLight.isAmbientLight) newLight.position.set(
			light.position.x || 0,
			light.position.y || 0,
			light.position.z || 0
		);

		newLight.castShadow = light.castShadow || false;
		newLight.visible = light.showLight || false;
		newLight.uid = light.uid;
		newLight.positioning = light.positioning;

		this.__lightsList.push(light);
		this.scene.add(newLight);
		this.parent.postMessage({callbackHandler: 'updated-light', dropped: {"items": this.__lightsList}}, '*');
	}

	showAdvancedRotation(isCentralizedRotation = false) {
		console.log(isCentralizedRotation);
		let err = this.__tcWrapper(function () {
			this.transformControls.setMode('rotate');
			this.transformControls.setSpace('local');
			if (isCentralizedRotation) this.transformControls.setSpace('world');
			this.transformControls.attach(this.objectContainer);
			this.transformControls.visible = true;
			this.gridHelper.visible = true;
			this.axesHelper.visible = true;
		}, this)();

		this.parent.postMessage({callbackHandler: 'shown-advanced-rotation', err: err}, '*');
	}

	hideAdvancedRotation() {
		let err = this.__tcWrapper(function () {
			if (this.transformControls.getMode() === 'rotate') {
				this.transformControls.detach(this.objectContainer);
				this.transformControls.visible = false;
				this.gridHelper.visible = false;
				this.axesHelper.visible = false;
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'hidden-advanced-rotation', err: err}, '*');
	}

	showAdvancedMovement() {
		let err = this.__tcWrapper(function () {
			this.transformControls.setMode('translate');
			this.transformControls.setSpace('world');
			this.transformControls.attach(this.objectContainer);
			this.transformControls.visible = true;
			this.gridHelper.visible = true;
			this.axesHelper.visible = true;
		}, this)();

		this.parent.postMessage({callbackHandler: 'shown-advanced-movement', err: err}, '*');
	}

	hideAdvancedMovement() {
		let err = this.__tcWrapper(function () {
			if (this.transformControls.getMode() === 'translate') {
				this.transformControls.detach(this.objectContainer);
				this.transformControls.visible = false;
				this.gridHelper.visible = false;
				this.axesHelper.visible = false;
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'hidden-advanced-movement', err: err}, '*');
	}

	rotate(xdeg, ydeg, zdeg, isCentralizedRotation) {
		let err = this.__tcWrapper(function () {

			this.centralizedRotation = isCentralizedRotation || false;

			let x = (xdeg / 180) * Math.PI;
			let y = (ydeg / 180) * Math.PI;
			let z = (zdeg / 180) * Math.PI;

			if (isCentralizedRotation) {
				this.worldRotationContainer.rotate(x, y, z);
				this.objectContainer.rotation.set(0, 0, 0);
			}
			else {
				this.objectContainer.rotation.set(xdeg, ydeg, zdeg);
				this.worldRotationContainer.rotate(0, 0, 0);
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'rotated', err: err}, '*');
	}

	move(xdeg, ydeg, zdeg) {
		let err = this.__tcWrapper(function () {
			this.objectContainer.position.set(xdeg, ydeg, zdeg);
		}, this)();

		this.parent.postMessage({callbackHandler: 'moved', err: err}, '*');
	}

	zoom(scale) {
		let err = this.__tcWrapper(function () {
			this.camera.zoom = scale;
			this.camera.updateProjectionMatrix();
		}, this)();

		this.parent.postMessage({callbackHandler: 'zoomed', err: err}, '*');
	}

	resetRotation() {
		let err = this.__tcWrapper(function () {
			this.objectContainer.rotation.set(0, 0, 0);
		}, this)();

		this.parent.postMessage({callbackHandler: 'reseted-rotation', err: err}, '*');
	}

	resetPosition() {
		let err = this.__tcWrapper(function () {
			this.objectContainer.position.set(0, 0, 0);
		}, this)();

		this.parent.postMessage({callbackHandler: 'reseted-position', err: err}, '*');
	}

	resetZoom() {
		let err = this.__tcWrapper(function () {
			this.camera.zoom = 1;
			this.camera.updateProjectionMatrix();
		}, this)();

		this.parent.postMessage({callbackHandler: 'reseted-zoom', err: err}, '*');
	}

	getScreenShot(width = this.container.offsetWidth, height = this.container.offsetHeight, mimetype = 'image/png') {
		let url = '';
		let err = this.__tcWrapper(function () {
			if (typeof arguments[0] === 'string') {
				mimetype = arguments[0];
				width = this.container.offsetWidth;
				height = this.container.offsetHeight;
			}
			let typeOfImage = mimetype.match(/([a-z])\w+/g)[1];
			let canvas2 = document.createElement('canvas');
			canvas2.width = width;
			canvas2.height = height;
			let context2 = canvas2.getContext('2d');


			context2.drawImage(this.renderer.domElement, 0, 0, width, height);
			url = canvas2.toDataURL(`image/${typeOfImage}`);

			let a = document.createElement('a');
			a.href = url;
			a.download = `screenshot.${typeOfImage}`;
			a.click();
		}, this)();

		this.parent.postMessage({callbackHandler: 'got-screenshot', dropped: url, err: err}, '*');
	}

	setFov(angle) {
		let err = this.__tcWrapper(function () {
			if (angle >= 1 && angle <= 179) {
				this.camera.fov = angle;
				this.camera.updateProjectionMatrix();
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'seted-fov', dropped: angle, err: err}, '*');
	}

	getCameralookAt() {
		let err = '';

		if (this.camera.position === undefined)
			err = 'Camera position is not defined';

		this.parent.postMessage({
			callbackHandler: 'got-cameralook-at', dropped: {
				"position": {x: this.camera.position.x, y: this.camera.position.y, z: this.camera.position.z},
				"target": this.orbitControls.target
			}, err: err
		}, '*');
	}

	setCameraLookAt(eye, target, duration = 2000, callback) {
		let tween = new TWEEN.Tween(target).to(eye, duration);
		tween.easing(TWEEN.Easing.Circular.Out);
		let that = this;
		tween.onStart(() => {
			this.orbitControls.enableRotate = false;
		});
		tween.onUpdate(function (pos) {
			that.camera.position.set(pos.x, pos.y, pos.z);
		});
		tween.onComplete(() => {
			this.orbitControls.enableRotate = true;
			this.parent.postMessage({callbackHandler: 'seted-cameralook-at'}, '*');
			if (callback instanceof Function) callback();
		});

		tween.start();
	}

	play() {
		let err = this.__tcWrapper(function () {
			if (!this.animationEnabled) {
				this.__animationsControllers.forEach(controller => {
					controller.action.paused = false;
				});
				this.animationEnabled = true;
				this.syncedAudioElements.forEach(el => {
					el.play();
				});
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'played', err: err}, '*');
	}

	pause() {
		let err = this.__tcWrapper(function () {
			if (this.animationEnabled) {
				this.__animationsControllers.forEach(controller => {
					controller.action.paused = true;
				});
				this.animationEnabled = false;
				this.syncedAudioElements.forEach(el => {
					el.pause();
				});
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'paused', err: err}, '*');
	}

	__setTimeAnimation(per) {
		this.__animationsControllers.forEach(controller => {
			if (controller.action) {
				controller.action.time = (parseFloat(per) / 100) * controller.action._clip.duration;
				playerCurrentTime.innerText = this.__timePrepare((controller.action.time * 1000).toFixed(0));

				this.syncedAudioElements.forEach(el => {
					el.currentTime = (parseFloat(per) / 100) * controller.action._clip.duration;
				});

			}
		});
	}

	seekTo(seconds) {
		let err = this.__tcWrapper(function () {
			this.__animationsControllers.forEach(controller => {
				if (controller.action) controller.action.time = parseFloat(seconds * 1000);
			});
			this.syncedAudioElements.forEach(el => {
				el.currentTime = seconds;
			});
		}, this)();

		this.parent.postMessage({callbackHandler: 'was-seek-to', err: err}, '*');
	}

	getCurrentTime() {
		let time = null;
		let duration = null;

		let err = this.__tcWrapper(function () {
			time = this.__animationsControllers[0].action.time;
			duration = this.__animationsControllers[0].action._clip.duration;
		}, this)();

		this.parent.postMessage({
			callbackHandler: 'got-current-time',
			dropped: {time: time, duration: duration},
			err: err
		}, '*');
	}

	getDuration() {
		let duration = null;
		let err = this.__tcWrapper(function () {
			duration = this.__animationsControllers[0].action._clip.duration;
		}, this)();
		this.parent.postMessage({
			callbackHandler: 'got-duration', dropped: {duration}, err: err}, '*');
	}

	setVolume(volume) {
		this.syncedAudioElements.forEach(el => el.volume = volume);
		this.unsyncedAudioElements.forEach(el => el.volume = volume);

		this.parent.postMessage({
			callbackHandler: 'seted-volume',
			dropped: {volume},
		}, '*');
	}

	setCycleMode(cycleMode) {
		let err = '';
		this.__animationsControllers.forEach(controller => {
			if (controller.action) {
				if (cycleMode === 'none') {
					controller.action.repetitions = 1;
				} else {
					controller.action.repetitions = Infinity;
				}
			}
			else err = 'Action in this file is not Define';
		});
		this.parent.postMessage({callbackHandler: 'seted-cycle-mode', err: err}, '*');
	}

	setSpeed(speed) {
		let err = this.__tcWrapper(function () {
			this.__animationsControllers.forEach(controller => {
				if (controller.action) {
					controller.action.timeScale = parseFloat(speed);
				}
			});
		}, this)();

		this.parent.postMessage({callbackHandler: 'seted-speed', dropped: speed, err: err}, '*');
	}

	intoFullScreen() {
		let err = this.__tcWrapper(function () {
			if (!document.fullscreenElement &&
				!document.mozFullScreenElement && !document.webkitFullscreenElement) {
				if (document.documentElement.requestFullscreen) {
					document.documentElement.requestFullscreen();
				} else if (document.documentElement.mozRequestFullScreen) {
					document.documentElement.mozRequestFullScreen();
				} else if (document.documentElement.webkitRequestFullscreen) {
					document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
				}
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'into-fullscreen', err: err}, '*');
	}

	outOfFullScreen() {
		let err = this.__tcWrapper(function () {
			if (document.cancelFullScreen) {
				document.cancelFullScreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen();
			}
		}, this)();

		this.parent.postMessage({callbackHandler: 'out-of-fullscreen', err: err}, '*');
	}

	setPolygonCount(count) {
		let mesh;
		let err = this.__tcWrapper(function () {
			if (this.objectContainer.children[0] instanceof THREE.Group) {
				mesh = this.objectContainer.children[0].children[0];
			} else {
				mesh = this.objectContainer.children[0];
			}

			const simplifyCoefficient = 1 - (count / mesh.convertedGeometry.vertices.length);
			const countOfSimplify = mesh.convertedGeometry.vertices.length * simplifyCoefficient;

			mesh.geometry = this.modifer.modify(mesh.convertedGeometry, countOfSimplify | 0);
			mesh.geometry.computeFaceNormals();
			mesh.geometry.computeVertexNormals();

		}, this)();

		this.parent.postMessage({callbackHandler: 'setPolygonCount', dropped: count, err: err}, '*');
	}

	getPolygonCount() {
		let mesh, count;
		let err = this.__tcWrapper(function () {
			if (this.objectContainer.children[0] instanceof THREE.Group) {
				mesh = this.objectContainer.children[0].children[0];
			} else {
				mesh = this.objectContainer.children[0];
			}

			if (mesh.geometry instanceof THREE.BufferGeometry) {
				count = new Geometry().fromBufferGeometry(mesh.geometry).vertices.length;
			} else {
				count = mesh.geometry.vertices.length;
			}
		}, this)();
		this.parent.postMessage({callbackHandler: 'getPolygonCount', dropped: count, err: err}, '*');
	}

	get3DModel() {
		let mesh, returnedObject;
		let err = this.__tcWrapper(function () {
			if (this.objectContainer.children[0] instanceof THREE.Group) {
				mesh = this.objectContainer.children[0].children[0];
			} else {
				mesh = this.objectContainer.children[0];
			}

			returnedObject = mesh.toJSON();
			const convertedGeometry = mesh.convertedGeometry.toJSON();
			convertedGeometry.name = "Geometry";
			delete convertedGeometry.metadata;
			returnedObject.object.geometry = convertedGeometry.uuid;
			returnedObject.geometries[0] = convertedGeometry;

		}, this)();
		this.parent.postMessage({callbackHandler: 'get3DModel', dropped: returnedObject, err: err}, '*');
	}

	resetViewer(hardReset) {
		if (hardReset) {
			let err = this.__tcWrapper(function () {
				this.objectContainer.traverse(model => {
					if (model instanceof THREE.Group) {
						const mesh = model.children[0];
						API.__adjustPosition(mesh);
						model.rotation.set(0, 0, 0);
					}
				});

				this.container.style.backgroundColor = '';
				this.container.style.background = '';

				if (this.__viewerObjects.cur_background.obj) {
					if (this.__viewerObjects.cur_background.type === 'image') {
						this.container.removeChild(this.__viewerObjects.cur_background.obj);
					} else if (this.__viewerObjects.cur_background.type === 'panorama' || this.__viewerObjects.cur_background.type === '3Dscene') {
						this.scene.remove(this.__viewerObjects.cur_background.obj);
					}
				}

				const removed_lights = [];

				this.scene.traverse(child => {
					if (child instanceof THREE.Light) removed_lights.push(child);
				});

				removed_lights.forEach(item => this.scene.remove(item));

				this.__lightsList.length = 0;

				this.addLights({
					'uid': '23423525u92u52598yu23hf',
					'type': 'ambient',
					'color': {'r': 1, 'g': 1, 'b': 1, 'a': 1},
					'intensity': 1,
					'positioning': 'scene',
					'castShadow': false,
					'showLight': true
				});

				this.addLights({
					"uid": "1acb4636bd8c4b31bbc329d425fa4a2d",
					"type": "directional",
					"color": {
						"r": 1,
						"g": 1,
						"b": 1,
						"a": 1
					},
					"intensity": 1,
					"position": {
						"x": 10,
						"y": 10,
						"z": 10
					},
					"castShadow": false,
					"positioning": "scene",
					"showLight": true
				});

				this.renderer.localClippingEnabled = false;
				this.objectContainer.position.set(0, 0, 0);
				this.objectContainer.children[0].children[0].geometry.computeBoundingBox();
				const bbox = this.objectContainer.children[0].children[0].geometry.boundingBox;
				let center = bbox.getCenter();
				this.objectContainer.children[0].children[0].position.sub(center);
				let size = new THREE.Vector3().subVectors(bbox.max, bbox.min);
				this.camera.position.set(0, 0, 3 * Math.max(Math.abs(size.x), Math.max(Math.abs(size.y), Math.abs(size.z))));
				this.camera.far = 10 * this.camera.position.z;
				this.camera.updateProjectionMatrix();
				this.orbitControls.update();

				let data = {
					callbackHandler: 'got-viewer-state',
					dropped: {
						"scene": {
							"background": this.__background,
							"cameraController": {
								"placement": {
									"position": {
										x: this.camera.position.x,
										y: this.camera.position.y,
										z: this.camera.position.z
									},
									"rotation": {
										x: this.camera.rotation.x,
										y: this.camera.rotation.y,
										z: this.camera.rotation.z
									},
									"target": this.orbitControls.target,
								},
								"camera": {
									"fov": this.camera.fov,
									"near": this.camera.near,
									"far": this.camera.far
								},
								"minDistance": this.orbitControls.minDistance,
								"maxDistance": this.orbitControls.maxDistance
							},
							"lights": {
								"items": this.__lightsList
							},
						},
						"models": this.__modelsList
					}
				};
			}, this)();

			this.parent.postMessage({callbackHandler: 'reseted-viewer', err: err}, '*');

		} else {
			let err = this.__tcWrapper(function () {
				let reset = this.__json;
				this.__background = {};
				this.__lightsList = [];
				this.__viewerObjects = {
					cur_background: {},
				};
				this.__animationsControllers = [];

				let deleted_obj = [];

				this.objectContainer.traverse(model => {
					deleted_obj.push(model);
				});

				deleted_obj.forEach((model) => {
					this.objectContainer.remove(model);
				});
				deleted_obj.length = 0;

				let cameraController = reset.scene.cameraController;

				this.camera.position.set(500, 100, 500);
				this.setCameraLookAt(cameraController.placement.position, cameraController.placement.target, 1500);

				this.camera.rotation.set(
					cameraController.placement.rotation.x,
					cameraController.placement.rotation.y,
					cameraController.placement.rotation.z
				);

				this.orbitControls.maxDistance = cameraController.maxDistance;
				this.orbitControls.minDistance = cameraController.minDistance;

				this.camera.lookAt(
					cameraController.placement.target.x,
					cameraController.placement.target.y,
					cameraController.placement.target.z
				);

				this.setBackground(reset.scene.background);

				this.__lightsList.length = 0;

				let deleted_lights = [];

				this.scene.traverse(light => {
					if (light instanceof THREE.Light) {
						deleted_lights.push(light);
					}
				});

				deleted_lights.forEach(light => {
					this.scene.remove(light);
				});

				deleted_lights.length = 0;

				reset.scene.lights.items.forEach(light => {
					this.addLights(light);
				});
			}, this)();

			this.parent.postMessage({callbackHandler: 'reseted-viewer', err: err}, '*');
		}
	}
}
