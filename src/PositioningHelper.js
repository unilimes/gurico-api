export default class PositioningHelper {
	constructor() {
		this.desc = "Class, that add some static methods for adjust positioning of objects on the scene.";
	}
}

PositioningHelper.calcBoundingBox = function (obj) {
	var minX = 0.0;
	var minY = 0.0;
	var minZ = 0.0;
	var maxX = 0.0;
	var maxY = 0.0;
	var maxZ = 0.0;
	var first = true;

	obj.traverse(function (mesh) {
		if (mesh instanceof THREE.Mesh) {
			mesh.geometry.computeBoundingBox();
			var bBox = mesh.geometry.boundingBox;

			if (first) {
				minX = bBox.min.x;
				minY = bBox.min.y;
				minZ = bBox.min.z;
				maxX = bBox.max.x;
				maxY = bBox.max.y;
				maxZ = bBox.max.z;

				first = false;
			} else {
				minX = Math.min(minX, bBox.min.x);
				minY = Math.min(minY, bBox.min.y);
				minZ = Math.min(minZ, bBox.min.z);
				maxX = Math.max(maxX, bBox.max.x);
				maxY = Math.max(maxY, bBox.max.y);
				maxZ = Math.max(maxZ, bBox.max.z);
			}
		}
	});

	return {
		min: new THREE.Vector3(minX, minY, minZ),
		max: new THREE.Vector3(maxX, maxY, maxZ),
		dem: new THREE.Vector3(maxX - minX, maxY - minY, maxZ - minZ)
	};
}

PositioningHelper.normalizePosition = function (obj, bb) {
	var scale = 1.0 / Math.max(bb.dem.x, Math.max(bb.dem.y, bb.dem.z));
	var center_pos = new THREE.Vector3(-(bb.max.x + bb.min.x) * 0.5, -(bb.max.y + bb.min.y) * 0.5, -(bb.max.y + bb.min.y) * 0.5);

	var matrix = new THREE.Matrix4();
	matrix.multiply(new THREE.Matrix4().makeTranslation(center_pos.x, center_pos.y, center_pos.z));

	obj.traverse(function (child) {
		if (child instanceof THREE.Mesh) {
			child.geometry.applyMatrix(matrix);
		}
	});
}
