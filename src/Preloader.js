export default class Preloader {
	constructor(elem) {
		this.elem = elem;
	}

	show(callback) {
		this.elem.classList.add('show');
		if (callback instanceof Function) callback();
	}

	hide(callback) {
		this.elem.classList.remove('show');
		if (callback instanceof Function) callback();
	}
}
