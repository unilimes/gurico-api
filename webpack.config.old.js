'use strict';

const NODE_ENV = process.NODE_ENV || 'development';

const path = require('path');
const webpack = require('webpack');

module.exports = {
    "entry":{
        "front": path.join(__dirname, './src/magicViewer.js'),
        "frame": path.join(__dirname, './src/ReceiverAPI.js')
    },
    "output": {
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/",
        filename: '[name]-bundle.js',
    },
    "plugins":[
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development',
        }),
        new webpack.ProvidePlugin({
            'THREE': '../libs/three.module.js',
            'TWEEN': '../libs/Tween.js'
        })
    ],
    devServer: {
        // host: '127.0.0.1',
        host: '192.168.1.142',
        port: 1430,
        inline: true,
        overlay: true,
        contentBase: path.join(__dirname, '/'),
    },
    devtool: "source-map"
};